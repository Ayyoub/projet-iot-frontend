import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { ApiuserService } from 'src/app/services/apiuser.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
})
export class AppSideRegisterComponent {

  users:User;

  constructor(private router: Router,private service:ApiuserService) {
    this.users=new User();
  }
  add(){
    
    this.service.adduser(this.users).subscribe({
      next:()=>{

        this.router.navigateByUrl('/')
      
    },
      error:(e)=>{console.log("Erreur d'ajout : "+ e.error.message)}
    })
  }
 
}
