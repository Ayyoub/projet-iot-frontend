import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { ApiuserService } from 'src/app/services/apiuser.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class AppSideLoginComponent {
  users:User;

  constructor(private router: Router,private service:ApiuserService) {
    this.users=new User();
  }

  login(){
    
    this.service.login(this.users).subscribe({
      next:()=>{

        this.router.navigateByUrl('/dashboard')
      
    },
      error:(e)=>{console.log("Erreur d'ajout : "+ e.error.message)}
    })
  }
}
