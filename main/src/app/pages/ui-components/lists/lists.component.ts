import { Component,OnInit  } from '@angular/core';
import {  dht11s } from 'src/app/models/dht11';
import { ApiService } from 'src/app/services/api.service';

export interface Section {
  name: string;
  updated: Date;
}

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
})
export class AppListsComponent  implements OnInit {
  projet: dht11s[] = [];
  
  constructor(private service:ApiService){
  
  }

  ngOnInit(): void {
      this.service.getAll().subscribe({
      next:(data)=>{
      this.projet=data;
      console.log(this.projet);
    },
      error:(e)=>{console.log("Erreur d'ajout : "+ e.error.message)}
    })
  }
  
  
  
}
