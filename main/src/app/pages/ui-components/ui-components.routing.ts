import { Routes } from '@angular/router';

// ui


import { AppListsComponent } from './lists/lists.component';
import { AppMenuComponent } from './menu/menu.component';


export const UiComponentsRoutes: Routes = [
  {
    path: '',
    children: [
     
     
      {
        path: 'lists',
        component: AppListsComponent,
      },
      {
        path: 'menu',
        component: AppMenuComponent,
      },
      
    ],
  },
];
