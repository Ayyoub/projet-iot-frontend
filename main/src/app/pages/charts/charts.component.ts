import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js/auto';
import { dht11s } from 'src/app/models/dht11';
import { ApiService } from 'src/app/services/api.service';
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})
export class ChartsComponent implements OnInit {
  projet: dht11s[] = [];
  tem:any[]=[]
  humi:any[]=[];
  dt:any[]=[]
  constructor(private service:ApiService) {
    
  }
  ngOnInit(): void {
    this.service.getAll().subscribe({
    next:(data)=>{

      this.projet = data;
      console.log(this.projet)
      for(let i=0;this.projet.length;i++){
        this.tem.push(this.projet[i].temp);
        this.humi.push(this.projet[i].hum);
      }
      console.log(this.tem)
      this.showchart()
      this.linechart()
  },
    error:(e)=>{console.log("Erreur d'ajout : "+ e.error.message)}
  })
}

showchart(){
  new Chart("myChart", {
    type: 'bar',
    data: {
      labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
      datasets: [{
        label: '# of Votes',
        data: [12, 19, 3, 5, 2, 3],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });
}
linechart(){
  
  new Chart("line", {
    type: 'line',
    data: { labels: ['1','2','3','4','5','6','7'],
      datasets: [{
        
        label: 'My First Dataset',
        data: [65, 59, 80, 81, 56, 55, 40],
        fill: false,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 205, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(201, 203, 207, 0.2)'
        ],
        borderColor: [
          'rgb(255, 99, 132)',
          'rgb(255, 159, 64)',
          'rgb(255, 205, 86)',
          'rgb(75, 192, 192)',
          'rgb(54, 162, 235)',
          'rgb(153, 102, 255)',
          'rgb(201, 203, 207)'
        ],
        borderWidth: 1
      }]
    },
    options: {
        scales: {
            y: {
                stacked: true
            }
        }
    }
});

}

 
}
