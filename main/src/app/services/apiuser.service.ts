import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';


@Injectable({
  providedIn: 'root'
})
export class ApiuserService {
  private apiUrl = 'https://projectiot.pythonanywhere.com/';

  constructor(private http:HttpClient) { }

  login(user:User){
      return this.http.post(this.apiUrl+"login",user);
  }
  
  adduser(user:User){
    console.log(user)
    return this.http.post(this.apiUrl+"signup",user );
  }
}
