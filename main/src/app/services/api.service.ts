import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { dht11s} from '../models/dht11';

@Injectable({
  providedIn: 'any'
})
export class ApiService {

  private apiUrl = 'https://projectiot.pythonanywhere.com/dht11list';

  constructor(private http:HttpClient) { }
  
  getAll():Observable<dht11s[]> {
    return this.http.get<dht11s[]>(this.apiUrl);
  }

 
}
